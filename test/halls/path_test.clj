(ns halls.path-test
  (:require [clojure.test :refer :all]
            [halls.path :refer :all]))

(deftest mirror?-test
  (let [mirror? #'halls.path/mirror?]
    (testing ":l and :r are mirrors, :s and :b are not"
     (are [x] (true? (mirror? x))
       :l
       :r)
     (are [x] (false? (mirror? x))
       :s
       :b))))

(deftest line-check?-test
  (let [space [0 0 :s]
        left  [0 0 :l]
        right [0 0 :r]
        line-check? #'halls.path/line-check?]
    (testing "Row number not equals"
      (is (false? (line-check? [space [0 1 :s] [1 1 :s]]))))
    (testing "Column number not equals"
      (is (false? (line-check? [space [1 0 :s] [1 1 :s]]))))
    (testing "space - mirror - space"
      (is (false? (line-check? [space left space])))
      (is (false? (line-check? [space right space]))))
    (testing "3 mirrors"
      (is (false? (line-check? [left left left])))
      (is (false? (line-check? [right right right])))
      (is (false? (line-check? [left right right])))
      (is (false? (line-check? [left left right])))
      (is (false? (line-check? [right right left])))
      (is (false? (line-check? [right left left]))))
    (testing "mirror - mirror - space"
      (is (false? (line-check? [left left space])))
      (is (false? (line-check? [right right space])))
      (is (false? (line-check? [left right space])))
      (is (false? (line-check? [right left space]))))
    (testing "mirror - space - mirror"
      (is (true? (line-check? [left space left])))
      (is (true? (line-check? [right space right])))
      (is (true? (line-check? [left space right])))
      (is (true? (line-check? [right space left]))))
    (testing "space - space - space"
      (is (true? (line-check? [space space space]))))
    (testing "space - space - mirror"
      (is (true? (line-check? [space space left])))
      (is (true? (line-check? [space space right]))))
    (testing "mirror - space - space"
      (is (true? (line-check? [left space space])))
      (is (true? (line-check? [right space space]))))))

(deftest turn-check?-test
  (let [turn-check? #'halls.path/turn-check?]
    (testing "Line should fail"
      (is (false? (turn-check? [[0 0 :s] [0 0 :l] [0 0 :s]]))))
    (testing "Wrong angle should fail"
      (is (false? (turn-check? [[0 0 :s] [1 0 :l] [1 1 :s]])))
      (is (false? (turn-check? [[0 1 :s] [1 1 :l] [1 2 :s]])))
      (is (false? (turn-check? [[0 1 :s] [1 1 :r] [1 0 :s]])))
      (is (false? (turn-check? [[2 1 :s] [1 1 :r] [1 2 :s]]))))
    (testing "Right angle should success"
      (is (true? (turn-check? [[0 0 :s] [1 0 :r] [1 1 :s]])))
      (is (true? (turn-check? [[0 1 :s] [1 1 :r] [1 2 :s]])))
      (is (true? (turn-check? [[0 1 :s] [1 1 :l] [1 0 :s]])))
      (is (true? (turn-check? [[2 1 :s] [1 1 :l] [1 2 :s]]))))))

(deftest shaped?-test
  (let [shaped? #'halls.path/shaped?]
    (testing "Wrong shapes - turn with wrong angle"
      (is (false? (shaped? [[0 0 :s] [1 0 :l] [1 1 :s]])))
      (is (false? (shaped? [[0 1 :s] [1 1 :l] [1 2 :s]])))
      (is (false? (shaped? [[0 1 :s] [1 1 :r] [1 0 :s]])))
      (is (false? (shaped? [[2 1 :s] [1 1 :r] [1 2 :s]])))
      (is (false? (shaped? [[0 0 :s] [0 0 :l] [0 0 :s]])))
      (is (false? (shaped? [[0 0 :s] [0 0 :r] [0 0 :s]]))))
    (testing "Wrong shapes - line"
      (is (false? (shaped? [[0 0 :s] [0 0 :l] [0 0 :s]])))
      (is (false? (shaped? [[0 0 :s] [0 0 :r] [0 0 :s]]))))
    (testing "Well shaped"
      (is (true? (shaped? [[0 0 :s] [1 0 :r] [1 1 :s]])))
      (is (true? (shaped? [[0 1 :s] [1 1 :r] [1 2 :s]])))
      (is (true? (shaped? [[0 1 :s] [1 1 :l] [1 0 :s]])))
      (is (true? (shaped? [[2 1 :s] [1 1 :l] [1 2 :s]])))
      (is (true? (shaped? [[0 0 :s] [0 1 :s] [0 2 :s]])))
      (is (true? (shaped? [[0 0 :s] [1 0 :s] [2 0 :s]])))
      (is (true? (shaped? [[0 0 :l] [0 1 :s] [0 2 :l]])))
      (is (true? (shaped? [[0 0 :l] [0 1 :s] [0 2 :r]]))))))

(deftest turn?-test
  (let [turn? #'halls.path/turn?]
    (testing "Line should fail"
      (is (false? (turn? [[0 0 :s] [0 1 :s] [0 2 :s]])))
      (is (false? (turn? [[0 0 :s] [0 1 :s] [0 2 :s]]))))
    (testing "Turn with right angle should success"
      (is (true? (turn? [[0 0 :s] [1 0 :r] [1 1 :s]])))
      (is (true? (turn? [[0 1 :s] [1 1 :r] [1 2 :s]])))
      (is (true? (turn? [[0 1 :s] [1 1 :l] [1 0 :s]])))
      (is (true? (turn? [[2 1 :s] [1 1 :l] [1 2 :s]]))))
    (testing "Turn with wrong angle should success too"
      (is (true? (turn? [[0 0 :s] [1 0 :l] [1 1 :s]])))
      (is (true? (turn? [[0 1 :s] [1 1 :l] [1 2 :s]])))
      (is (true? (turn? [[0 1 :s] [1 1 :r] [1 0 :s]])))
      (is (true? (turn? [[2 1 :s] [1 1 :r] [1 2 :s]]))))))

(defonce rows [[:b :b :b :b]
               [:b :r :l :b]
               [:b :s :l :s]
               [:b :s :s :b]
               [:b :s :b :b]])

(defonce path-1 [[2 3] [2 2] [2 1] [3 1] [4 1]])
(defonce path-2 [[2 3] [2 2] [1 2] [1 1] [2 1] [3 1] [4 1]])

(deftest attach-node-value-test
  (let [attach-node-value #'halls.path/attach-node-value]
    (is (= '([2 3 :s] [2 2 :l] [2 1 :s] [3 1 :s] [4 1 :s])
           (attach-node-value path-1 rows)))
    (is (= '([2 3 :s] [2 2 :l] [1 2 :l] [1 1 :r] [2 1 :s] [3 1 :s] [4 1 :s])
         (attach-node-value path-2 rows)))))

(deftest valid?-test
  (testing "Path not starts or ends with a `:s` should fail"
    (is (false? (valid? [[2 2] [2 1] [3 1] [4 1]] rows)))
    (is (false? (valid? [[2 3] [2 2] [2 1] [3 1]] rows))))
  (testing "Path not shaped well should fail"
    (is (false? (valid? path-1 rows))))
  (testing "Path shaped well should fail"
    (is (false? (valid? path-2 rows)))))

(deftest swith-mirror-direction-test
  (let [switch-mirror-direction #'halls.path/switch-mirror-direction]
    (is (= [0 0 :r] (switch-mirror-direction [0 0 :l])))
    (is (= [0 0 :l] (switch-mirror-direction [0 0 :r])))
    (is (thrown? Exception (switch-mirror-direction [0 0 :b])))
    (is (thrown? Exception (switch-mirror-direction [0 0 :s])))))

(deftest calculate-mirror-wrong-states-test
  (let [calculate-mirror-wrong-states #'halls.path/calculate-mirror-wrong-states]
    (testing "Wrong path will just return passed `compete` number."
      (is (= Integer/MAX_VALUE
             (calculate-mirror-wrong-states path-1 rows Integer/MAX_VALUE)))
      (is (= 1 (calculate-mirror-wrong-states path-1 rows 1))))
    (testing "For any path which can be fixed, return the number of changed states."
      (is (= 3 (calculate-mirror-wrong-states path-2 rows Integer/MAX_VALUE))))
    (testing "Compete number will win if more wrong states"
      (is (= 2 (calculate-mirror-wrong-states path-2 rows 2))))))

(defonce rows-2
  [[:b :b :b :b]
   [:b :r :l :b]
   [:b :s :l :s]
   [:b :r :l :b]
   [:b :s :b :b]])

(defonce paths-2
  '([[2 3] [2 2] [2 1] [3 1] [4 1]]
    [[2 3] [2 2] [3 2] [3 1] [4 1]]
    [[2 3] [2 2] [1 2] [1 1] [2 1] [3 1] [4 1]]))

(deftest calculate-minimal-mirror-wrong-states-test
  (let [f calculate-minimal-mirror-wrong-states]
    (testing "Empty paths should return Integer/MAX_VALUE"
      (is (= Integer/MAX_VALUE (f [] rows))))
    (testing "Skip computation if only one state needs to be changed for any path"
      (is (= 1 (f paths-2 rows-2))))))
