(ns halls.core-test
  (:require [clojure.test :refer :all]
            [halls.core :refer :all]
            [clojure.java.io :as io]))

(def invalid-entry-test
  "3 5\n*.**.\n*..**\n****.\n0 0")

(deftest compute-test
  (testing "Invalid entries"
    (is (= :invalid-entries (ffirst (compute invalid-entry-test)))))
  (testing "Compute halls"
    (is (= '(3 0 3 2 -1 2)
           (compute (slurp (io/resource "test.txt")))))))
