(ns halls.bfs-test
  (:require [clojure.test :refer :all]
            [halls.bfs :refer :all]))

(defonce adjacents
  {[1 1] '([1 2] [2 1])
   [1 2] '([1 1] [2 2])
   [2 1] '([1 1] [2 2] [3 1])
   [2 2] '([1 2] [2 1] [2 3] [3 2])
   [2 3] '([2 2])
   [3 1] '([2 1] [3 2] [4 1])
   [3 2] '([2 2] [3 1])
   [4 1] '([3 1])})

;; entry node
(defonce a [2 3])
;; exit node
(defonce b [4 1])

(defonce g (->Graph adjacents))

(deftest bfs-search-test
  (is (= (bfs-search g a)
         '([[2 3]]
           [[2 3] [2 2]]
           [[2 3] [2 2] [1 2]]
           [[2 3] [2 2] [2 1]]
           [[2 3] [2 2] [3 2]]
           [[2 3] [2 2] [1 2] [1 1]]
           [[2 3] [2 2] [2 1] [1 1]]
           [[2 3] [2 2] [2 1] [3 1]]
           [[2 3] [2 2] [3 2] [3 1]]
           [[2 3] [2 2] [1 2] [1 1] [2 1]]
           [[2 3] [2 2] [2 1] [1 1] [1 2]]
           [[2 3] [2 2] [2 1] [3 1] [3 2]]
           [[2 3] [2 2] [2 1] [3 1] [4 1]]
           [[2 3] [2 2] [3 2] [3 1] [2 1]]
           [[2 3] [2 2] [3 2] [3 1] [4 1]]
           [[2 3] [2 2] [1 2] [1 1] [2 1] [3 1]]
           [[2 3] [2 2] [3 2] [3 1] [2 1] [1 1]]
           [[2 3] [2 2] [1 2] [1 1] [2 1] [3 1] [3 2]]
           [[2 3] [2 2] [1 2] [1 1] [2 1] [3 1] [4 1]]
           [[2 3] [2 2] [3 2] [3 1] [2 1] [1 1] [1 2]])))
  (testing "Make sure no same path"
    (is (= (distinct (bfs-search g a))
           (bfs-search g a)))))

(deftest all-paths-test
  (is (= (all-paths g a b)
         '([[2 3] [2 2] [2 1] [3 1] [4 1]]
           [[2 3] [2 2] [3 2] [3 1] [4 1]]
           [[2 3] [2 2] [1 2] [1 1] [2 1] [3 1] [4 1]])))
  (testing "Make sure all paths are distinct"
    (is (= (distinct (all-paths g a b))
           (all-paths g a b))))
  (testing "Paths from node `a` to node `b` should be the reverse of from `b` to `a`"
    (is (= (->> (all-paths g a b)
                (map reverse))
           (all-paths g b a)))))

(deftest get-shortest-path-test
  (is (= (get-shortest-path g a b)
         [[2 3] [2 2] [2 1] [3 1] [4 1]])))
