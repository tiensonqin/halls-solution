(ns halls.parse-test
  (:require [clojure.test :refer :all]
            [halls.parse :refer :all]))

(deftest get-rows-cols-test
  (let [f #'halls.parse/get-rows-cols]
    (is (= [3 3] (f "3 3")))
    (is (= [3 5] (f "3 5")))
    (is (thrown? Exception (f "foobar")))))

(deftest get-entries-test
  (let [f #'halls.parse/get-entries]
    (testing "No entries in the row"
      (is (empty? (f [5 4] 0 [:b :b :b :b])))
      (is (empty? (f [5 4] 1 [:b :r :l :b])))
      (is (empty? (f [5 4] 3 [:b :s :s :b]))))
    (testing "Only one entry in the row"
      (is (= '([2 3]) (f [5 4] 2 [:b :s :l :s])))
      (is (= '([4 1]) (f [5 4] 4 [:b :s :b :b]))))
    (testing "Two entries in the row"
      (is (= '([4 1] [4 3]) (f [5 4] 4 [:b :s :b :s]))))))

(defonce rows
  [[:b :b :b :b]
   [:b :r :l :b]
   [:b :s :l :s]
   [:b :s :s :b]
   [:b :s :b :b]])

(deftest parse-test
  (testing "`0 0` will be empty"
    (is (empty? (parse "0 0"))))
  (testing "Success parsing should return `rc`, `rows` and two `entries`."
      (is (= (parse "5 4
****
*\\/*
*./.
*..*
*.**
0 0
")
             [{:rc [5 4]
               :rows rows
               :entries '([2 3] [4 1])}]))))

(deftest get-near-nodes-test
  (let [f #'halls.parse/get-near-nodes]
    (is (empty? (f [0 0] rows)))
    (is (= (f [0 1] rows) '([1 1])))
    (is (= (f [1 1] rows) '([1 2] [2 1])))
    (is (= (f [2 2] rows) '([1 2] [2 1] [2 3] [3 2])))
    (is (= (f [2 3] rows) '([2 2])))))

(deftest build-adjacents-test
  (let [f #'halls.parse/build-adjacents]
    (is (= (f rows)
           {[1 1] '([1 2] [2 1])
            [1 2] '([1 1] [2 2])
            [2 1] '([1 1] [2 2] [3 1])
            [2 2] '([1 2] [2 1] [2 3] [3 2])
            [2 3] '([2 2])
            [3 1] '([2 1] [3 2] [4 1])
            [3 2] '([2 2] [3 1])
            [4 1] '([3 1])}))))
