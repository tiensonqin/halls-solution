(ns halls.core
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.pprint :refer [pprint]]
            [halls.parse :as parse]
            [halls.bfs :as bfs]
            [halls.path :as path]))

;; Outline:
;; 1. Parse text into halls which type is
;; (map of (row,column) * entries * rows * adjacents) list.
;; 2. For each hall, construct a modified breadth-first search graph,
;;    then get all of the paths from entry `A` to exit `B`.
;; 3. Count minimal mirrors which states need to be changed.

;; TODO:
;; 1. Coverage testing, quickcheck.
;; 2. Improve performance. Using mutable data structures, avoid useless loops.
;; 3. Try different traversal, for example dfs and a-star path-finding.
;; 4. Visualization.

(defn compute
  "Parse input text to a sequence of halls, and calculate minimal mirrors
  which states need to changed for each hall."
  [input]
  (let [halls (-> (parse/parse input)
                  (parse/attach-adjacents))]
    (for [{:keys [_rc rows entries adjacents] :as hall} halls]
      (if (and (= (count entries) 2) (not= (first entries) (second entries)))
        (let [g (bfs/->Graph adjacents)
              paths (bfs/all-paths g (first entries) (second entries))]
          (cond
            (empty? paths)      ; no way to make it
            -1

            (some #(path/valid? % rows) paths)
            0

            :else
            (let [c (path/calculate-minimal-mirror-wrong-states paths rows)]
              (if (= c Integer/MAX_VALUE)
                -1
                c))))
        [:invalid-entries hall]))))

(comment
  (def test-input
    (slurp (io/resource "test.txt")))

  (pprint (-> (parse/parse test-input)
              (parse/attach-adjacents)))

  (pprint (compute test-input)))
