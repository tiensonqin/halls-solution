(ns halls.parse
  (:require [clojure.string :as s]))

(defn- char->type
  [c]
  (case c
    \* :b                               ; block
    \. :s                               ; space
    \/ :l                               ; turn left
    \\ :r                               ; turn right
    nil))

(defn- get-rows-cols
  "Extract both rows and columns from `line`."
  [line]
  (try
    (if (string? line)
     (some->> (s/split line #" ")
              (mapv #(Integer/parseInt %))))
    (catch Exception e
      (throw (ex-info "Wrong rows and cols:" {:line line})))))

(defn- indexed
  [coll]
  (map-indexed vector coll))

(defn- get-entries
  "Extract entries (include exit) from `row`.
  A node is an entry or an exit if it equals to `:s` and positioned on the
  border of the rows."
  [[rows_n cols_n] row-idx row]
  (let [entries (->> (filter (fn [[idx v]] (= :s v)) (indexed row))
                     (map first))]
    (when (seq entries)
      (let [entries (if (contains? #{0 (dec rows_n)} row-idx)
                      ;; top or bottom border
                      entries
                      ;; left or right border
                      (filter #{0 (dec cols_n)} entries))]
        (map #(conj [row-idx] %) entries)))))

(defn parse
  "Parse input `s` into halls."
  [s]
  (let [lines (s/split-lines s)]
    (reduce
     (fn [acc line]
       (cond
         (= "0 0" line)                   ; end of parsing
         acc

         (Character/isDigit (first line)) ; new hall
         (let [result (into acc [{:rc (get-rows-cols line)
                                  :rows []
                                  :entries []}])]
           result)

         :else                            ; aggregate rows for current hall
         (let [row (mapv char->type line)
               cur-hall-idx (dec (count acc))
               {:keys [rc entries rows] :as cur-hall} (nth acc cur-hall-idx)
               new-entries (get-entries rc (count rows) row)]
           (assoc acc cur-hall-idx {:rc rc
                                    :rows (into rows [row])
                                    :entries (concat entries new-entries)}))))
     []
     lines)))

(defn- get-near-nodes
  [[row col] rows]
  (let [f (fn [row col]
            (let [v (get-in rows [row col])]
              (if (and v (not= :b v))   ; remove blocks
                [row col])))]
    (->> [
          (f (dec row) col) ; top
          (f row (dec col)) ; left
          (f row (inc col)) ; right
          (f (inc row) col) ; bottom
          ]
         (remove nil?))))

(defn build-adjacents
  [rows]
  (letfn [(build-row-adjacents [acc [row_n row]]
            (reduce
             (fn [acc [col_n v]]
               (if (= v :b)
                 acc
                 (assoc acc [row_n col_n]
                        (get-near-nodes [row_n col_n] rows))))
             acc
             (indexed row)))]
    (reduce build-row-adjacents {} (indexed rows))))

(defn attach-adjacents
  [halls]
  (for [hall halls]
    (assoc hall :adjacents (build-adjacents (:rows hall)))))
