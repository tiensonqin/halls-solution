(ns halls.bfs
  (:require [clojure.set :as set]))

;; A modified breadth-first search algorithm.

(defrecord Graph [adjacents])

(defn- bfs
  [adjacents q]
  (lazy-seq
   (when (seq q)
     (let [current (first q)
           current-adjacents (->> (get adjacents (last current))
                                  ;; here we only consider current path
                                  ;; instead of all of the visisted nodes
                                  (remove (set current)))]
       (cons current
             (bfs adjacents
                  (into (pop q)
                        (map #(conj current %) current-adjacents))))))))

(defn bfs-search [graph a]
  (let [q (conj clojure.lang.PersistentQueue/EMPTY [a])]
    (bfs (:adjacents graph) q)))

(defn all-paths [graph a b]
  (filter #(= (last %) b) (bfs-search graph a)))

(defn get-shortest-path [graph a b]
  (first (all-paths graph a b)))
