(ns halls.sample
  (:require [clojure.string :as s]))

(defn border?
  [i j row col]
  (or (contains? #{0 (dec row)} i)
      (contains? #{0 (dec col)} j)))

(defn rand-char
  []
  (rand-nth [\* \. \/ \\]))

(defn generate
  [row col]
  (let [entries #{[0 (rand-int (dec col))]
                  [(dec row) (rand-int (dec col))]}
        chars (for [i (range 0 row)
                    j (range 0 col)]
                (cond
                 (contains? entries [i j])
                 \.

                 (border? i j row col)
                 \*

                 :else
                 (rand-char)))]
    (->> (partition col chars)
         (map #(apply str %))
         (cons (str row " " col))
         (s/join "\n"))))

(defn generate-samples
  []
  (let [halls (for [i (range 10)]
                (generate 8 8))         ; TODO: [10, 10] is very slow to compute.
        result (-> (s/join "\n" halls)
                   (str "\n0 0"))]
    (spit "resources/samples.txt" result)))

(comment
  (generate-samples)

  (-> (slurp (clojure.java.io/resource "samples.txt"))
      (halls.core/compute)))
