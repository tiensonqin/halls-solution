(ns halls.path)

;; We check 3 nodes each time to make sure they were shaped well.
(defonce nodes-num 3)

(defn- mirror?
  "Whether current node is a `mirror`."
  [node]
  (contains? #{:l :r} node))

(defn- line-check?
  "Ensure the three nodes are on the same line and light can beam through."
  [[[r1 c1 v1] [r2 c2 v2] [r3 c3 v3]]]
  (and
   (or (= r1 r2 r3) (= c1 c2 c3))
   ;; There are three possibilities.
   (or (= :s v1 v2)                     ; 1. the first two nodes are spaces
       (= :s v2 v3)                     ; 2. the last two nodes are spaces
       (and (mirror? v1)                ; 3. mirror then space then mirror
            (= v2 :s)
            (mirror? v3)))))

(defn- get-angle
  "Get the angle between two nodes in degree."
  [[r1 c1 _v1] [r2 c2 _v2]]
  (/ (* (Math/atan2 (- c2 c1) (- r2 r1)) 180)
     Math/PI))

(defn- turn-check?
  "Ensure the three nodes are a turn and the mirror has the right direction."
  [[n1 [_r2 _c2 v2] n3]]
  (let [angle (get-angle n1 n3)]
    (or (and (= v2 :r) (contains? #{45.0 -135.0} angle))
        (and (= v2 :l) (contains? #{-45.0 135.0} angle)))))

(defn- shaped?
  "Whether these three nodes are a valid line or turn."
  [nodes]
  (cond
    (line-check? nodes)
    true

    (turn-check? nodes)
    true

    :else
    false))

(defn- turn?
  "Whether these three nodes are a turn, notice that the mirror might has
  the `wrong` direction."
  [[[r1 c1 v1] [r2 c2 v2] [r3 c3 v3]]]
  (and (mirror? v2)
       (and (not= r1 r2 r3)
            (not= c1 c2 c3))))

(defn- attach-node-value
  [path rows]
  (map (fn [node] (into node [(get-in rows node)])) path))

(defn valid?
  "Whether the path is valid, every `nodes-num` successive nodes should be shaped well."
  [path rows]
  (let [path (attach-node-value path rows)]
    ;; Ensure this path both begins and ends with a `:s`.
    (if (= :s (last (first path)) (last (last path)))
      (loop [path path]
        (cond
          (< (count path) nodes-num)        ; done
          true

          (shaped? (take nodes-num path))
          (recur (rest path))

          :else
          false))
      false)))

(defn- switch-mirror-direction
  [[row col direction]]
  (case direction
    :l [row col :r]
    :r [row col :l]
    (throw (ex-info "Mirror wrong direction: "
                    {:row row
                     :col col
                     :direction direction}))))

(defn- calculate-mirror-wrong-states
  [path rows compete]
  (let [path (attach-node-value path rows)]
    (loop [path path c 0]
      (cond
        (< (count path) nodes-num)
        c

        (>= c compete) ; no need to compute more since `compete` already wins
        compete

        :else
        (let [nodes (take nodes-num path)]
          (cond
            (shaped? nodes) ; skip shaped
            (recur (rest path) c)

            (turn? nodes)   ; turn with wrong angle
            (let [mirror-node (second path)
                  path' (cons (switch-mirror-direction mirror-node)
                              (drop 2 path))]
              (recur path' (inc c)))

            :else           ; no way to make it
            compete))))))

(defn calculate-minimal-mirror-wrong-states
  "Calculate minimal mirrors which states need to be changed."
  [paths rows]
  (loop [paths paths c Integer/MAX_VALUE]
    (if (empty? paths)
      c
      (let [path (first paths)
            c' (calculate-mirror-wrong-states path rows c)]
        (if (= 1 c') ; skip computation since `1` is the least.
          1
          (recur (rest paths)
                 (min c c')))))))
