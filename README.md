# Halls solution

## Outline
1. Parse text into halls which type is
   (map of (row,column) * entries * rows * adjacents) list.
2. For each hall, construct a modified breadth-first search graph, then get all of the paths from entry `A` to exit `B`.
3. Count miminal mirrors which states need to be changed.

## License

Copyright © 2018 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
